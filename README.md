# 2D Engine Nulu

A handcrafted 2d engine being written in Ruby for personal use. I'm worried mostly about design and readabilty for educational purposes (but performance will naturally be a factor as well).

## Road Map

* Collision::SAT (private)
* Collision::collides?
* Collision::

## Geometry

Geometry group:

* Point
* Segment
* Polygon
* Particular Polygons: Circle, Rectangle, Square
* Compound Polygons: Union, Intersection, Complement
* Collision
  * collides? (between polygons only)
  * intersects? included? (between shapes interpreted as lines, segments and points)

Point and Segment are primitives to build more complex types. The main type of this pseudo-module is Polygon, from where each particular shape will be derived from (curves will be aproximated). A Polygon will have to be convex so that the Collision module gives proper results. The idea is to use Compound Polygons to create concave shapes, but that idea is very hazy right now.

Thinking ahead to a future Physics group, I'll think of bodies with a certain shape and a position. Originally, I was thinking of splitting both properties (AKA, think bare shapes were 'position-less' in a way). I thought that was clean, but it doesn't make a lot of sense. First of all, there's a performance issue of having to offset shapes every time you want to check for collision. Secondly, after the offset, at the end of the day you'd still be checking shapes for collision, which means that you're considering shapes to effectively have a position in the plane, and not in a vacuum. So my previously thought clean semantics of 'position-less' shapes are in fact not that clean (looking at shapes like this, moving a shape would mean transforming it into a different shape, so it's just a weird way of looking at the problem). Bottomline is, I ain't doing that. But this means shapes now will have to store a 'center'.

Initialization of these types will be done via the type they depend on: points will be initialized with numbers, segments and polygons with points. A 'make' method will be provided to support other forms of initialization.

The Collision module will hold all the code related to calculating intersections and collisions between different objects. The 'collides?' rutine will only deal with Polygons, since only them are considered actual 2D shapes (even though it may make sense to ask wheter a segment is colliding with a shape). In the 'intersects?' and 'included?' rutine we see polygons as outlines, so it accepts points and lines as well as shapes/polygons.
