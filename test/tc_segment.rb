require "minitest/autorun"
require_relative "../lib/nulu"

class TestSegment < Minitest::Unit::TestCase
  def test_init
    a = Nulu::Point.new(0, 1)
    b = Nulu::Point.new(1, 1)
    s = Nulu::Segment.new(a, b)
    assert_equal a, s.a
    assert_equal b, s.b
    s = Nulu::Segment.new()
    assert_equal Nulu::Point.new(0, 0), s.a
    assert_equal Nulu::Point.new(0, 0), s.b
  end

  def test_comparisons
    l1 = Nulu::Segment.make(:center => Nulu::Point.new(1, 0),
                            :direction => Nulu::Point.new(1, 1))
    l2 = Nulu::Segment.make(:center => Nulu::Point.new(2, 1),
                            :direction => Nulu::Point.new(-1, -1))
    assert l1.parallel?(l1)
    assert !l1.perpendicular?(l1)
    assert !l1.perpendicular?(l2)
    assert l1.parallel?(l2)
    assert l2.parallel?(l1)
    l3 = Nulu::Segment.make(:center => Nulu::Point.new(1, 0),
                            :direction => Nulu::Point.new(1, -1))
    assert !l1.parallel?(l3)
    assert !l3.parallel?(l1)
    assert l1.perpendicular?(l3)
    assert l3.perpendicular?(l1)
    assert l2.perpendicular?(l3)
  end
end
