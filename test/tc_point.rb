require "minitest/autorun"
require_relative "../lib/nulu"

class TestPoint < Minitest::Unit::TestCase
  def test_init
    p = Nulu::Point.new(2, 5)
    assert_equal 2, p.x
    assert_equal 5, p.y
  end

  def test_make
    p = Nulu::Point.make(:angle => -Math::PI, :norm => 1)
    assert_in_delta (-1), p.x 
    assert_in_delta 0, p.y
    p = Nulu::Point.make(:angle => -Math::PI*1/4, :norm => Math::sqrt(8))
    assert_in_delta 2, p.x 
    assert_in_delta (-2), p.y
  end

  def test_polar
    p = Nulu::Point.new(-1, -1)
    assert_in_delta (Math::PI*5.0/4.0), p.angle
    assert_in_delta Math::sqrt(2), p.norm
  end

  def test_modifiers
    p = Nulu::Point.new()
    p.norm = 1
    p.angle = Math::PI/4
    assert_in_delta p.x, 0.707
    assert_in_delta p.y, 0.707
    p.point_to(3, 4)
    assert_equal p.x, 3
    assert_equal p.y, 4
    p.apply(Nulu::Point.new(2, 1))
    assert_equal p.x, 5
    assert_equal p.y, 5
  end

  def test_operators
    v1 = Nulu::Point.new(2, 2)
    v2 = Nulu::Point.new(1, -3)
    r = v1 + v2
    assert_equal Nulu::Point.new(3, -1), r
    r = v1 - v2
    assert_equal Nulu::Point.new(1, 5), r
    r = v1 * v2
    assert_equal (-4), r
    r = v2 * -0.5
    assert_equal Nulu::Point.new(-0.5, 1.5), r
    assert_raises(RuntimeError) { v1 * 'a' }
    r = v1 / 2
    assert_equal Nulu::Point.new(1, 1), r
    r = v1 ** v2
    assert_equal (-8), r
  end

  def test_projection
    p1 = Nulu::Point.new(1, 1).unit()
    p2 = Nulu::Point.new(1, 0)
    assert_in_delta Math.cos(Math::PI/4), p1.sproject(p2)
    assert_in_delta Math.cos(Math::PI/4), p2.sproject(p1)
    assert Nulu::Point.new(Math.cos(Math::PI/4), 0), p1.vproject(p2)
    p2 = Nulu::Point.new(1, 1).unit() / 2
    assert_in_delta 1, p1.sproject(p2)
    assert_in_delta 0.5, p2.sproject(p1)
    assert p1, p1.vproject(p2)
    assert p2, p2.vproject(p1)
  end


  def test_misc
    p = Nulu::Point.new(0, -1)
    assert !p.zero?
    assert_in_delta 1, p.unit.norm
    assert_in_delta p.angle, p.unit.angle
    b = p.dup()
    p.trim(0.2)
    assert_in_delta 0.2, p.norm
    assert_in_delta b.angle, p.angle
    p = Nulu::Point.new(0, 0)
    assert p.zero?
    assert !b.zero?
  end

  def test_comparisons
    v1 = Nulu::Point.new(2, 1)
    v2 = Nulu::Point.new(5, 5)
    assert Math::sqrt(3**2+4**2), v1.distance(v2)
  end
end
