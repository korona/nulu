require "minitest/autorun"
require_relative "../lib/nulu"

class TestCollision < Minitest::Unit::TestCase
  def test_scalar_intersection
    l1 = Nulu::Segment.make(:center => Nulu::Point.new(0, 0),
                            :direction => Nulu::Point.new(-0.5, -1))
    t1, t2 = Nulu::Collision::scalar_intersection(l1, l1)
    assert_nil t1
    l2 = Nulu::Segment.make(:center => Nulu::Point.new(0, 3),
                            :direction => Nulu::Point.new(1, -1))
    t1, t2 = Nulu::Collision::scalar_intersection(l1, l2)
    assert_equal (-2), t1
    assert_equal 1, t2
    t2, t1 = Nulu::Collision::scalar_intersection(l2, l1)
    assert_equal (-2), t1
    assert_equal 1, t2
    l3 = Nulu::Segment.make(:center => Nulu::Point.new(1, 0),
                            :direction => Nulu::Point.new(1, -1))
    t3, t2 = Nulu::Collision::scalar_intersection(l3, l2)
    assert_nil t3
  end

  def test_linear_intersection
    a = Nulu::Point.new(0, 1)
    b = Nulu::Point.new(1, 1)
    c = Nulu::Point.new(1, 0)
    d = Nulu::Point.new(0, 0)
    e = Nulu::Point.new(2, 2)
    s1 = Nulu::Segment.new(a, c)
    s2 = Nulu::Segment.new(d, e)
    assert_equal Nulu::Point.new(0.5, 0.5),
                 Nulu::Collision::linear_intersection(s1, s2)
    s1 = Nulu::Segment.new(d, c);
    s2 = Nulu::Segment.new(a, b);
    assert_nil Nulu::Collision::linear_intersection(s1, s2)
    s1 = Nulu::Segment.new(a, c)
    s2 = Nulu::Segment.new(e, b)
    assert_nil Nulu::Collision::linear_intersection(s1, s2)
  end
end
