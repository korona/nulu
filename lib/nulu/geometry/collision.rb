module Nulu

  module Collision
    def self.collides?(a, b)
    end
  
    def self.includes?(a, b)
    end

    def self.intersects?(a, b)
    end

    # El plan es usar las x_intersection y otras más para implementar
    # ya directo la colisión entre polígonos. La joda va a reducir
    # toda colisión a la colisión de un polígono. Además, por otro 
    # lado, las 'formas' que van a ser los cuerpos, van a ser todas
    # polígonos convexos (namely elipses)

    def self.linear_intersection(la, lb)
      t1, t2 = scalar_intersection(la, lb)
      if t1 && (t1 >= 0 - EPS && t1 <= 1 + EPS) &&
               (t2 >= 0 - EPS && t2 <= 1 + EPS)
        return la.a + (la.b - la.a) * t1
      else
        return nil
      end
    end
  
    def self.scalar_intersection(la, lb)
      c = la.center
      v = la.direction
      d = lb.center
      w = lb.direction
      ndet = v.x * w.y - v.y * w.x
      if ndet.abs > EPS
        return ((w.y * (d.x - c.x) - w.x * (d.y - c.y)) / ndet),
               ((v.y * (d.x - c.x) - v.x * (d.y - c.y)) / ndet)
      else
        return nil
      end
    end
  end

end
