require_relative "geometry/point"
require_relative "geometry/segment"
require_relative "geometry/polygon"
require_relative "geometry/collision"
